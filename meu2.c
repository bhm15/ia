#include <stdlib.h>
#include <stdio.h>
#include <time.h>

typedef struct {
  int nlinhas;
  int ncolunas;
  int ncores;
  int **mapa;
} tmapa;


void libera_mapa(tmapa* m){
	int i;
	for(i = 0; i < m->nlinhas; ++i){
		free(m->mapa[i]);
	}
	free(m->mapa);
	free(m);
}

void carrega_mapa(tmapa *m) {
  int i, j;

  scanf("%d", &(m->nlinhas));
  scanf("%d", &(m->ncolunas));
  scanf("%d", &(m->ncores));
  m->mapa = (int**) malloc(m->nlinhas * sizeof(int*));
  for(i = 0; i < m->nlinhas; i++) {
    m->mapa[i] = (int*) malloc(m->ncolunas * sizeof(int));
    for(j = 0; j < m->ncolunas; j++)
      scanf("%d", &(m->mapa[i][j]));
  }
}

void mostra_mapa(tmapa *m) {
  int i, j;

  printf("%d %d %d\n", m->nlinhas, m->ncolunas, m->ncores);
  for(i = 0; i < m->nlinhas; i++) {
    for(j = 0; j < m->ncolunas; j++)
      if(m->ncores > 10)
	printf("%02d ", m->mapa[i][j]);
      else
	printf("%d ", m->mapa[i][j]);
    printf("\n");
  }
}

void mostra_mapa_cor(tmapa *m) {
  int i, j;
  char* cor_ansi[] = { "\x1b[0m",
		       "\x1b[31m", "\x1b[32m", "\x1b[33m",
		       "\x1b[34m", "\x1b[35m", "\x1b[36m",
		       "\x1b[37m", "\x1b[30;1m", "\x1b[31;1m",
		       "\x1b[32;1m", "\x1b[33;1m", "\x1b[34;1m",
		       "\x1b[35;1m", "\x1b[36;1m", "\x1b[37;1m" };

  if(m->ncores > 15) {
    mostra_mapa(m);
    return;
  }
  printf("%d %d %d\n", m->nlinhas, m->ncolunas, m->ncores);
  for(i = 0; i < m->nlinhas; i++) {
    for(j = 0; j < m->ncolunas; j++)
      if(m->ncores > 10)
	printf("%s%02d%s ", cor_ansi[m->mapa[i][j]], m->mapa[i][j], cor_ansi[0]);
      else
	printf("%s%d%s ", cor_ansi[m->mapa[i][j]], m->mapa[i][j], cor_ansi[0]);
    printf("\n");
  }
}

void pinta(tmapa *m, int l, int c, int fundo, int cor) {
  m->mapa[l][c] = cor;
  if(l < m->nlinhas - 1 && m->mapa[l+1][c] == fundo)
    pinta(m, l+1, c, fundo, cor);
  if(c < m->ncolunas - 1 && m->mapa[l][c+1] == fundo)
    pinta(m, l, c+1, fundo, cor);
  if(l > 0 && m->mapa[l-1][c] == fundo)
    pinta(m, l-1, c, fundo, cor);
  if(c > 0 && m->mapa[l][c-1] == fundo)
    pinta(m, l, c-1, fundo, cor);
}

void pinta_mapa(tmapa *m, int cor) {
  if(cor == m->mapa[0][0])
    return;
  pinta(m, 0, 0, m->mapa[0][0], cor);
}


void copia_mapa(tmapa* m , tmapa* new){
	new->nlinhas = m->nlinhas;
	new->ncolunas = m->ncolunas;
	new->ncores = m->ncores;
	
	int i,j;
	new->mapa = malloc(sizeof(int*)*m->nlinhas);
	for(i = 0; i < m->nlinhas; ++i){
		new->mapa[i] = malloc(sizeof(int)*m->ncolunas);
		for(j = 0; j < m->nlinhas; ++j){
			new->mapa[i][j] = m->mapa[i][j];
		}
	}
}


int contaAreaRec(tmapa* m, int l, int c, int cor){
	int cont = 1;
	m->mapa[l][c] = -1;
	
	if(l+1 < m->nlinhas && m->mapa[l+1][c] == cor)
		cont+=contaAreaRec(m,l+1,c,cor);
	if(l-1 > 0 && m->mapa[l-1][c] == cor)
		cont+=contaAreaRec(m,l-1,c,cor);
	if(c+1 < m->ncolunas && m->mapa[l][c+1] == cor)
		cont+=contaAreaRec(m,l,c+1,cor);
	if(c-1 > 0 && m->mapa[l][c-1] == cor)
		cont+=contaAreaRec(m,l,c-1,cor);
	
	return cont;
}


int contaArea(tmapa* m){
	tmapa* copiaM = malloc(sizeof(tmapa));
	copia_mapa(m, copiaM);
	int ret = contaAreaRec(copiaM,0,0,m->mapa[0][0]);
	libera_mapa(copiaM);
	return ret;
}

int numeroCores(tmapa* m){
	int i,j;
	int* auxCores = malloc(sizeof(int)*m->ncores);
	
	for(i = 0; i < m->ncores; ++i){
		auxCores[i] = 0;
	}
	
	for(i = 0; i < m->nlinhas; ++i){
		for(j = 0; j < m->ncolunas; ++j){
			auxCores[ m->mapa[i][j]-1 ] +=1;
		}
	}
	
	int cores = 0;
	
	for(i = 0; i < m->ncores; ++i){
		if(auxCores[i] != 0)
			cores++;
	}
	free(auxCores);
	return cores-1;
}

int heuristica(tmapa* m){
	//~ return m->nlinhas+m->ncolunas - diagonalScore(m);
	//~ fprintf(stderr,"%d\n",contaArea(m));
	//~ return contaArea(m);
	//~ return m->nlinhas*m->ncolunas-(contaConq(m));
	//~ fprintf(stderr,"%d\n",contaArea(m));
	//~ return m->nlinhas*m->ncolunas-(contaArea(m));
	return numeroCores(m);
	//~ return maiorMenorCaminho(m);
}






typedef struct nodoArvore{
	tmapa* mapa;
	struct nodoArvore** filhos;
	struct nodoArvore* pai;
	int nivel;
	int cor;
	int h;
} nodoArvore;

void liberaArvore(nodoArvore* nodo){
	int i;
	
	
	for(i = 0; i < nodo->mapa->ncores; i++){
		if(nodo->filhos[i] != NULL){
			liberaArvore(nodo->filhos[i]);
		}
		
		
	}
	free(nodo->filhos);
	libera_mapa(nodo->mapa);
	//~ fprintf(stderr,"%ld ",&nodo);
	
	free(nodo);
	
}
void relaciona(nodoArvore* pai, nodoArvore* filho, int cor){
	pai->filhos[cor-1] = (nodoArvore*)filho;
	filho->pai = pai;
}

nodoArvore* criaNodo(nodoArvore* pai, int cor){
	
	tmapa* novoMapa = malloc(sizeof(tmapa));
	copia_mapa(pai->mapa, novoMapa);
	nodoArvore* ret = (nodoArvore*)malloc(sizeof(nodoArvore));
	ret->mapa = novoMapa;
	
	ret->filhos = malloc(sizeof(nodoArvore*)*pai->mapa->ncores);
	int i;
	for(i = 0; i < pai->mapa->ncores; ++i){
		ret->filhos[i] = NULL;
	}
	ret->nivel = pai->nivel+1;
	ret->cor = cor;
	relaciona(pai,ret,cor);
	pinta_mapa(ret->mapa,cor);
	
	ret->h = heuristica(ret->mapa);
	return ret;
}



typedef struct filaNodo{
	nodoArvore* n;
	struct filaNodo* next;
} filaNodo;

void insereFilaNodo(filaNodo* f, nodoArvore* n){
	while(f->next != NULL){
		f = f->next;
	}
	f->next = malloc(sizeof(filaNodo));
	f->next->n = n;
	f->next->next = NULL;
}

void removeFilaNodo(filaNodo* f, nodoArvore* n){
	
	while(f->next != NULL && f->next->n != n){
		f = f->next;
		//~ fprintf(stderr,"%ld	",f->n->nivel);
	}
	
	//~ fprintf(stderr,"\n%ld\n",n);
	
	if(f->next == NULL)
		return;
		
	filaNodo* aux = f->next;
	f->next = f->next->next;
	free(aux);
}

void expandeNodo(filaNodo* borda, nodoArvore* pai){
	int i;
	for(i = 0; i < pai->mapa->ncores; ++i){
		nodoArvore* novo = criaNodo(pai,i+1);
		
		insereFilaNodo(borda,novo);
	}
	removeFilaNodo(borda,pai);
}















int main(int argc, char **argv) {
	int i;
	tmapa* m = malloc(sizeof(tmapa));
	//~ tfronteira *f;
	carrega_mapa(m);
	
	//~ grafo = malloc(sizeof(int*)*m->nlinhas*m->ncolunas);
	//~ for(i = 0; i < m->nlinhas; ++i){
		//~ grafo[i] = malloc(sizeof(int)*m->nlinhas*m->ncolunas);
	//~ }
	
	
	//~ f = aloca_fronteira(m);

	nodoArvore* raiz = malloc(sizeof(nodoArvore));
	raiz->nivel = 0;
	
	raiz->filhos = malloc(sizeof(nodoArvore*)*m->ncores);
	raiz->mapa = m;
	for(i = 0; i < m->ncores; ++i){
		raiz->filhos[i] = NULL;
	}
	
	raiz->pai = NULL;
	raiz->h = heuristica(raiz->mapa);

	filaNodo* borda = malloc(sizeof(filaNodo));
	borda->next = NULL;
	borda->n = raiz;
	
	//~ mostra_mapa_cor(raiz.mapa);
	

	expandeNodo(borda,raiz);
	
	nodoArvore* escolheNodo;
	i = 0;
	int bestScore, auxScore, valorH;
	filaNodo* aux;
	//~ fronteira_mapa(m, f);
	valorH = 1;
	escolheNodo = raiz;
	
	while(escolheNodo->h > 0){
		
		aux = borda;
		//~ bestScore = aux->next->n->mapa->nlinhas*aux->next->n->mapa->ncolunas;
		escolheNodo = aux->next->n;
		while(aux->next != NULL){
			aux = aux->next;
			//~ fprintf(stderr,"%d	%d\n",aux->n->h,aux->n->nivel);
			//~ fprintf(stderr,"%d ",aux);
			if(aux->n->h + aux->n->nivel <= escolheNodo->h + escolheNodo->nivel){
				escolheNodo = aux->n;
				//~ fprintf(stderr,"%d: %d %d\n",bestScore, auxScore, aux->n->nivel);
			}
			
			
		}
		//~ fprintf(stderr,"\n\n");
		//~ fprintf(stderr,"%d\n",escolheNodo->h);
		//~ mostra_mapa_cor(escolheNodo->mapa);
		//~ printf("\n");
		expandeNodo(borda,escolheNodo);
		i++;
		//~ fronteira_mapa(escolheNodo->mapa, f);
	}
    //~ mostra_mapa_cor(escolheNodo->mapa);
    
    
    int passos = escolheNodo->nivel;
    int* ordem = malloc(sizeof(int)*passos);
    
	printf("%d\n",passos);
    while(escolheNodo != raiz){
		//~ printf("%d\n",escolheNodo->cor);
		ordem[escolheNodo->nivel-1] = escolheNodo->cor;
		escolheNodo = escolheNodo->pai;
	}
	for(i = 0; i < passos; ++i){
		printf("%d ",ordem[i]);
	}
	printf("\n");
    liberaArvore(raiz);
    //~ libera_fronteira(f);
    
    aux = borda;
	filaNodo* prev;
	while(aux->next != NULL){
		prev = aux;
		aux = aux->next;
		if(prev != NULL)
			free(prev);
	}
  
	return 0; 
}
