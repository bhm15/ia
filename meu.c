#include <stdlib.h>
#include <stdio.h>
#include "mapa.h"
#include <time.h>
#include <stdlib.h>

int** grafo;


int caminhoMaximo(tmapa* m){
	tfronteira* f;
	f = aloca_fronteira(m);
	fronteira_mapa(m, f);
	
	int i,j;
	int indiceGrafo;
	int indiceGrafo2;
	
	for(i = 0; i < f->tamanho; ++i){
		fprintf(stderr,"%d  << \n",f->pos[4]);
		indiceGrafo = f->pos[i].l*m->ncolunas  + f->pos[i].c;
		grafo[indiceGrafo][0] = 1;
		grafo[0][indiceGrafo] = 1;
	}
	
	
	for(i = 0; i < m->nlinhas; i++){
		for(j = 0; j < m->ncolunas; j++){
			printf("%d ",grafo[i][j]);
		}
		printf("\n");
	}
	return 0;
}


int maiorMenorCaminho(tmapa* m){
	tmapa* copiaM = aloca_mapa(m);
	tmapa* copia2M = aloca_mapa(m);
	copia_mapa(m, copiaM);
	pinta_mapa(copiaM,-1);
	
	
	int i, j;
	int maiorDis = 0;


	//~ for(i = 0; i < m->nlinhas; ++i){
		//~ for(j = 0; j < m->ncolunas; ++j){
			
		//~ }
	//~ }
	
	
	libera_mapa(copiaM);
	libera_mapa(copia2M);
	return 0;
}

int contaArea(tmapa* m){
	tmapa* copiaM = aloca_mapa(m);
	copia_mapa(m, copiaM);
	int ret = m->nlinhas*m->ncolunas-
	contaAreaRec(copiaM,0,0,m->mapa[0][0]);
	libera_mapa(copiaM);
	fprintf(stderr,"%d\n",ret);
	return ret;
}

int contaAreaRec(tmapa* m, int l, int c, int cor){
	int cont = 1;
	m->mapa[l][c] = -1;
	
	if(l+1 < m->nlinhas && m->mapa[l+1][c] == cor)
		cont+=contaAreaRec(m,l+1,c,cor);
	if(l-1 > 0 && m->mapa[l-1][c] == cor)
		cont+=contaAreaRec(m,l-1,c,cor);
	if(c+1 < m->ncolunas && m->mapa[l][c+1] == cor)
		cont+=contaAreaRec(m,l,c+1,cor);
	if(c-1 > 0 && m->mapa[l][c-1] == cor)
		cont+=contaAreaRec(m,l,c-1,cor);
	
	return cont;
}

int diagonalScore(tmapa *m) {
	int i, max;
	
	tfronteira* f = aloca_fronteira(m);
	
	max = 0;
	fronteira_mapa(m, f);
	for(i = 0; i < f->tamanho; ++i){
		if(max < f->pos[i].l+f->pos[i].c){
			max = f->pos[i].l+f->pos[i].c;
		}
	}
	libera_fronteira(f);
	if(max > m->ncolunas)
		max = m->ncolunas;
	if(max > m->nlinhas)
		max = m->nlinhas;
	
	return max;
}

int contaConq(tmapa *m) {
	//~ int count = 1;
	//~ int cor = m->mapa[0][0];
	//~ int fundo = cor; 
	//~ printf("%ld %ld\n",l,c);
	//~ if(l < m->nlinhas - 1 && m->mapa[l+1][c] == fundo)
	//~ count+=contaConq(m, l+1, c);
	//~ if(c < m->ncolunas - 1 && m->mapa[l][c+1] == fundo)
	//~ count+=contaConq(m, l, c+1);
	//~ if(l > 0 && m->mapa[l-1][c] == fundo)
	//~ count+=contaConq(m, l-1, c);
	//~ if(c > 0 && m->mapa[l][c-1] == fundo)
	//~ count+=contaConq(m, l, c-1);

	int i, l, c;

	int* scores = malloc(sizeof(int)*m->ncores);
	for(i = 0; i < m->ncores; ++i){
		scores[i] = 0;  
	}
	for(l = 0; l < m->nlinhas; ++l){
		for(c = 0; c < m->ncolunas; ++c){
			scores[m->mapa[l][c]-1]++;
		}  
	}
	int iMax = 0;
	for(i = 0; i < m->ncores; ++i){
		if(scores[iMax] < scores[i]){
			iMax = i;
		}
	}
	return scores[iMax];
}






int heuristica(tmapa* m){
	//~ return m->nlinhas+m->ncolunas - diagonalScore(m);
	//~ fprintf(stderr,"%d\n",contaArea(m));
	//~ return contaArea(m);
	//~ return m->nlinhas*m->ncolunas-(contaConq(m));
	return m->nlinhas*m->ncolunas-(contaArea(m));
	//~ return maiorMenorCaminho(m);
}


typedef struct nodoArvore{
	tmapa* mapa;
	struct nodoArvore** filhos;
	struct nodoArvore* pai;
	int nivel;
	int cor;
} nodoArvore;

void liberaArvore(nodoArvore* nodo){
	int i;
	
	
	for(i = 0; i < nodo->mapa->ncores; i++){
		if(nodo->filhos[i] != NULL){
			liberaArvore(nodo->filhos[i]);
		}
		
		
	}
	free(nodo->filhos);
	libera_mapa(nodo->mapa);
	//~ fprintf(stderr,"%ld ",&nodo);
	
	free(nodo);
	
}
void relaciona(nodoArvore* pai, nodoArvore* filho, int cor){
	pai->filhos[cor-1] = (nodoArvore*)filho;
	filho->pai = pai;
}

nodoArvore* criaNodo(nodoArvore* pai, int cor){
	
	tmapa* novoMapa = aloca_mapa(pai->mapa);
	copia_mapa(pai->mapa, novoMapa);
	nodoArvore* ret = (nodoArvore*)malloc(sizeof(nodoArvore));
	ret->mapa = novoMapa;
	
	ret->filhos = malloc(sizeof(nodoArvore*)*pai->mapa->ncores);
	int i;
	for(i = 0; i < pai->mapa->ncores; ++i){
		ret->filhos[i] = NULL;
	}
	ret->nivel = pai->nivel+1;
	ret->cor = cor;
	relaciona(pai,ret,cor);
	pinta_mapa(ret->mapa,cor);
	
	return ret;
}



typedef struct filaNodo{
	nodoArvore* n;
	struct filaNodo* next;
} filaNodo;

void insereFilaNodo(filaNodo* f, nodoArvore* n){
	while(f->next != NULL){
		f = f->next;
	}
	f->next = malloc(sizeof(filaNodo));
	f->next->n = n;
	f->next->next = NULL;
}

void removeFilaNodo(filaNodo* f, nodoArvore* n){
	
	while(f->next != NULL && f->next->n != n){
		f = f->next;
	}
	if(f->next == NULL)
		return;
	filaNodo* aux = f->next;
	f->next = f->next->next;
	free(aux);
}

void expandeNodo(filaNodo* borda, nodoArvore* pai){
	int i;
	for(i = 0; i < pai->mapa->ncores; ++i){
		nodoArvore* novo = criaNodo(pai,i+1);
		insereFilaNodo(borda,novo);
	}
	removeFilaNodo(borda,pai);
}


int main(int argc, char **argv) {
	
   
  
  
	int i;
	tmapa* m = malloc(sizeof(tmapa));
	tfronteira *f;
	carrega_mapa(m);
	
	grafo = malloc(sizeof(int*)*m->nlinhas*m->ncolunas);
	for(i = 0; i < m->nlinhas; ++i){
		grafo[i] = malloc(sizeof(int)*m->nlinhas*m->ncolunas);
	}
	
	
	f = aloca_fronteira(m);

	nodoArvore* raiz = malloc(sizeof(nodoArvore));
	raiz->nivel = 0;
	
	raiz->filhos = malloc(sizeof(nodoArvore*)*m->ncores);
	raiz->mapa = m;
	for(i = 0; i < m->ncores; ++i){
		raiz->filhos[i] = NULL;
	}
	
	raiz->pai = NULL;


	filaNodo* borda = malloc(sizeof(filaNodo));
	borda->next = NULL;
	borda->n = raiz;

	//~ mostra_mapa_cor(raiz.mapa);
	

	expandeNodo(borda,raiz);
	
	nodoArvore* escolheNodo;
	i = 0;
	int bestScore, auxScore, valorH;
	filaNodo* aux;
	fronteira_mapa(m, f);
	valorH = 1;
	escolheNodo = raiz;
	while(f->tamanho){
		
		aux = borda;
		bestScore = aux->next->n->mapa->nlinhas*aux->next->n->mapa->ncolunas;
		while(aux->next != NULL){
			aux = aux->next;
			
			
			//~ auxScore = contaConq(aux->n->mapa);
			valorH = heuristica(aux->n->mapa);
			caminhoMaximo(aux->n->mapa);
			auxScore = valorH+aux->n->nivel; //h(x) + g(x)
			if(auxScore <= bestScore){
				bestScore = auxScore;
				escolheNodo = aux->n;
				//~ fprintf(stderr,"%d: %d %d\n",bestScore, auxScore, aux->n->nivel);
			}
			
			
		}
		//~ fprintf(stderr,"%d\n",bestScore);
		expandeNodo(borda,escolheNodo);
		i++;
		fronteira_mapa(escolheNodo->mapa, f);
	}
    //~ mostra_mapa_cor(escolheNodo->mapa);
    
    
    int passos = escolheNodo->nivel;
    int* ordem = malloc(sizeof(int)*passos);
    
	printf("%d\n",passos);
    while(escolheNodo != raiz){
		//~ printf("%d\n",escolheNodo->cor);
		ordem[escolheNodo->nivel-1] = escolheNodo->cor;
		escolheNodo = escolheNodo->pai;
	}
	for(i = 0; i < passos; ++i){
		printf("%d ",ordem[i]);
	}
	printf("\n");
    liberaArvore(raiz);
    libera_fronteira(f);
    
    aux = borda;
	filaNodo* prev;
	while(aux->next != NULL){
		prev = aux;
		aux = aux->next;
		if(prev != NULL)
			free(prev);
	}
  
	return 0; 
}
  
